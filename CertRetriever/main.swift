//
//  main.swift
//  CertRetriever
//
//  Created by Admin on 5/8/17.
//  Copyright © 2017 NoMAD. All rights reserved.
//

import Foundation
import SystemConfiguration

// Variables

var machine = ""
var password = ""
var useKeychain = false
var host = ""
var template = ""
var noQuit = true
var dateOverride = false

let sem = DispatchSemaphore(value: 0)

// Queues

let myWorkQueue = DispatchQueue(label: "menu.nomad.CertRetreiver.background_work_queue", attributes: [])

// functions

func printHelp() {
    print("Name\r")
    print("\tCertRetriever - Machine Cert Retrieval Utility\r")
    print("Synopsis\r")
    print("\tCertRetriever [-m machine name] [-p password] [-P] [-h WebCA] [-t template] [-H] [-d] [-D]\r")
    print("Description\r")
    print("\tCertRetriever will use the AD machine account password stored in the System Keychain of a bound machine to connect to a Windows Certificate Authority and generate a machine certificate.\r")
    print("\t-m machine")
    print("\t\tThis specifies the AD computer account for this system.")
    print("\t-p password")
    print("\t\tThis specifies the password to use for the machine account.")
    print("\t-P")
    print("\t\tOverrides -p and pulls the password for the machine directly from the System Keychain. This will be the safest way to use the machine account password.")
    print("\t-h WebCA")
    print("\t\tThe URL of your Web CA. Just enter the naked DNS name, e.g. dc1.nomad.test, as there is no need for https:// or the trailing path.")
    print("\t-t template")
    print("\t\tThis specifies the certificate template to use.")
    print("\t-H")
    print("\t\tPrints this help message.")
    print("\t-d")
    print("\t\tPrints the date the last cert will expire.")
    print("\t-D")
    print("\t\tOverrides the protection to not get a new certificate if an existing one has more than 30 days before expiration.")
}

func testSite(caURL: String, completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) {
    
    let request = NSMutableURLRequest(url: URL(string: caURL)!)
    
    request.httpMethod = "GET"
    
    let session = URLSession.shared
    session.dataTask(with: request as URLRequest, completionHandler: completionHandler).resume()
}

func getCert(_ alerts: Bool) {
    
    var myResponse: Int?
    
    // TODO: check to see if the SSL Certs are trusted, otherwise we'll fail
    
    // pre-flight to ensure valid URL and template
    
    var certCATest = host
    let certTemplateTest = template
    
    if ( certCATest != "" && certTemplateTest != "" ) {
        
        let lastExpireTemp = defaults.object(forKey: Preferences.lastCertificateExpiration) ?? ""
        var lastExpire: Date?
        
        if (String(describing: lastExpireTemp)) == "" {
            lastExpire = Date.distantPast as Date
        } else {
            lastExpire = lastExpireTemp as? Date
        }
        
        
        if (lastExpire?.timeIntervalSinceNow)! > 2592000 {
            print("Cert will not expire for a while.")
            NSApp.terminate(nil)
        }
        
        
        // check for http://
        
        if !certCATest.contains("http://") || !certCATest.contains("https://") {
            certCATest = "https://" + certCATest
        }
        
        /*
         
         // preflight that there aren't SSL issues
         var caTestWait = true
         var caSSLTest = true
         myWorkQueue.async(execute: {
         testSite(caURL: certCATest, completionHandler: { (data, response, error) in
         
         if (error != nil) {
         caSSLTest = false
         }
         caTestWait = false
         }
         )})
         
         while caTestWait {
         RunLoop.main.run(mode: RunLoopMode.defaultRunLoopMode, before: Date.distantFuture)
         myLogger.logit(.debug, message: "Waiting for CA test to complete.")
         }
         
         if !caSSLTest {
         
         myLogger.logit(.base, message: "CA connection error")
         } else {
         
         let certCARequest = WindowsCATools(serverURL: certCATest, template: certTemplateTest)
         certCARequest.certEnrollment()
         }
         
         } else {
         
         myLogger.logit(.base, message: "CA configuration error")
         }
         */
        
        let certCARequest = WindowsCATools(serverURL: certCATest, template: certTemplateTest)
        certCARequest.certEnrollment()
    } else {
        print("CA configuration error")
    }
}

func getADSettings() {
    
    let net_config = SCDynamicStoreCreate(nil, "net" as CFString, nil, nil)
    let ad_info = [ SCDynamicStoreCopyValue(net_config, "com.apple.opendirectoryd.ActiveDirectory" as CFString)]
    if ad_info[0] != nil {
        let adDict = ad_info[0]! as! NSDictionary
        machine = adDict["TrustAccount"] as! String
    }
}

// The real app!

// Get all the options

// -m -- machine name
// -p -- machine password
// -P -- pull password from the System Keychain
// -h -- Web CA host
// -t -- cert template
// -H -- help

let args = CommandLine.arguments

if args.count == 1 {
    printHelp()
    noQuit = false
}

for item in 0...(args.count - 1) {
    
    switch args[item] {
    case "-m":
        machine = args[item + 1]
    case "-p":
        password = args[item + 1]
    case "-P":
        useKeychain = true
    case "-h":
        host = args[item + 1]
    case "-t":
        template = args[item + 1]
    case "-H":
        printHelp()
        noQuit = false
    case "-d":
        if machine == "" {
            print("Getting machine name from SystemConfiguration")
            getADSettings()
        }
        let keychain = KeychainUtil()
        let expiration = keychain.findCertExpiration(machine, defaultNamingContext: "")
        if expiration == nil {
            print("No cert matching that machine name.")
        } else {
            print("Last certificate expires: \(expiration!)")
        }
        noQuit = false
    case "-D":
        dateOverride = true
    default:
        break
    }
}


if noQuit {
    
    print("--Starting CertRetriever--")
    
    if machine == "" {
        print("Getting machine name from SystemConfiguration")
        getADSettings()
    }
    
    // pull the password from the keychain
    
    if useKeychain || password == "" {
        print("Getting password")
        let keychainUtil = KeychainUtil()
        password = keychainUtil.findPassword(machine)
    }
    
    let keychain = KeychainUtil()
    let expiration = keychain.findCertExpiration(machine, defaultNamingContext: "") ?? NSDate() as Date
    
    if (expiration.timeIntervalSinceNow) > 60 * 60 * 24 * 30 && !dateOverride {
        print("Certificate will not expire for more than 30 days from now. Use -D to override this warning.")
        print("Last certificate expires: \(expiration)")
    } else {
        dateOverride = true
    }
    
    if dateOverride {
        // get tickets
        
        print("Getting tickets")
        
        let kerb = KerbUtil()
        
        let error = kerb.getKerbCredentials(password, machine)
        
        if error != nil {
            print("Error: \(error)")
        }
        
        // get a cert
        
        print("Getting Cert")
        
        getCert(true)
        
        // set up a deadline
        
        let deadline = DispatchTime.now() + .seconds(60)
        
        sem.wait(timeout: deadline)
        
        let keychain = KeychainUtil()
        let expiration = keychain.findCertExpiration(machine, defaultNamingContext: "")
        
        if expiration == nil {
            print("No cert matching that machine name.")
        } else {
            print("Last certificate expires: \(expiration!)")
        }

    }
}


