***Name***

	CertRetriever - Machine Cert Retrieval Utility

***Synopsis***

	CertRetriever [-m machine name] [-p password] [-P] [-h WebCA] [-t template] [-H] [-d] [-D]

***Description***

	CertRetriever will use the AD machine account password stored in the System Keychain of a bound machine to connect to a Windows Certificate Authority and generate a machine certificate.
	-m machine
		This specifies the AD computer account for this system.
	-p password
		This specifies the password to use for the machine account.
	-P
		Overrides -p and pulls the password for the machine directly from the System Keychain. This will be the safest way to use the machine account password.
	-h WebCA
		The URL of your Web CA. Just enter the naked DNS name, e.g. dc1.nomad.test, as there is no need for https:// or the trailing path.
	-t template
		This specifies the certificate template to use.
	-H
		Prints this help message.
	-d
		Prints the date the last cert will expire.
	-D
		Overrides the protection to not get a new certificate if an existing one has more than 30 days before expiration.